//step 1 - create actions constants
export const EXAMPLE_INCREMENT = 'EXAMPLE/INCREMENT';
export const EXAMPLE_DECREMENT = 'EXAMPLE/DECREMENT';
export const EXAMPLE_MULTIPLY = 'EXAMPLE/MULTIPLY ';


//step 2 - create actions creators 
export const increment = () => {
    return{
        type: EXAMPLE_INCREMENT
    }
}

export const decrement = () => {
    return{
        type: EXAMPLE_DECREMENT
    }
}

export const multiply = () => {
    return{
        type: EXAMPLE_MULTIPLY,
    }
}

//step 3 - export actions 

export const actions = {
    increment,
    decrement,
    multiply,
}