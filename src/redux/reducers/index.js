import { combineReducers } from 'redux'
import counterReducer from './example'

const reducer = combineReducers ({
    counter: counterReducer
});

export default reducer;