import {
  EXAMPLE_INCREMENT,
  EXAMPLE_DECREMENT,
  EXAMPLE_MULTIPLY,
} from '../actions/example';

const initialState = 0;

const counterReducer = (state = initialState, action) => {
  switch (action.type) {
    case EXAMPLE_INCREMENT:
      return state + 1;
    case EXAMPLE_DECREMENT:
      return state - 1;
    case EXAMPLE_MULTIPLY:
      return state * action.payload;
    default:
      return state;
  }
};

export default counterReducer;